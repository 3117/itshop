<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-1.5">
               
            </div>
            <div class="col-2.5">
                <b>
                    <h4 class="posttext">Fear Of Missing Out?
                </b></h4>
                <p style="color: gray;" class="posttext">Get the latest deals, updates and more.</p>
            </div>
            <div class="col-5">
                <div class="input-group input">
                    <input type="search" class="form-control rounded" placeholder="Your email address"
                        aria-label="Search" aria-describedby="search-addon" style="height: 3rem;">
                    <button type="button" class="btn btn-danger btn2">Subscribe </button>
                </div>
            </div>
            <div class="col-3 media">
                FOLLOW US
                <i class="fa fa1 fa-facebook-f"></i>
                <i class="fa fa1 fa-twitter"></i>
                <i class="fa fa1 fa-instagram"></i>
            </div>
        </div>
    </div>
</div>
<div class="container details">
    <div class="row">
        <div class="col-2">
            <h3 style="line-height: 10rem;">COMPANY</h3>
            <p>About us</p>
            <p>Careers</p>
            <p>Affiliates</p>
            <p>Blog</p>
            <p>Contact Us</p>
        </div>
        <div class="col-2">
            <h3 style="line-height: 10rem;">SHOP</h3>
            <p>Televisions</p>
            <p>Fridges</p>
            <p>Washing Machines</p>
            <p>Fans</p>
            <p>Air Conditioners</p>
            <p>Laptops</p>
        </div>
        <div class="col-2">
            <h3 style="line-height: 10rem;">HELP</h3>
            <p>Customers Service</p>
            <p>My Account</p>
            <p>Find a Store</p>
            <p>Legal & Privacy</p>
            <p>Contact</p>
            <p>Gift Card</p>
        </div>
        <div class="col-2">
            <h3 style="line-height: 10rem;">ACCOUNT</h3>
            <p>My Profile</p>
            <p>My Order History</p>
            <p>My Wish List</p>
            <p>Order Tracking</p>
        </div>
        <div class="col-4 contactinfo">
            <h3 style="line-height: 10rem;">CONTACT INFO</h3>
            <p>About us</p>
            <p>Contact us</p>
            <p>Blog</p>
        </div>
    </div>
</div>
<hr>


    <hr>
    Copyright &copy; 2022 Quantox Technology. All rights reserved
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>