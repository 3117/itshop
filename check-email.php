<?php

echo json_encode([
    'valid' =>  (bool)filter_var($_GET['email'], FILTER_VALIDATE_EMAIL),
]);
