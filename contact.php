

      <?php include_once 'header.php'; ?>
      
      <main>
    <div id="contact-container">
        <section id="store-information">
            <h2>STORE INFORMATION</h2>
            <div>
                
                <p>Leo Technoloy <br> United States</p>
            </div>
            <hr>
            <div>
               
                <p>Email us: <br> demo@demo.com</p>
            </div>
        </section>
        <section id="main-container">
            <h2>Our location</h2>
            <section id="contact-map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2875.5879320773943!2d20.35232301544488!3d43.885088579113976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475772388c694f27%3A0x4841a3dfdeb39771!2sQuantox%20Technology!5e0!3m2!1ssr!2srs!4v1650057295616!5m2!1ssr!2srs" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </section>
            <section id="contact-form">
                <h2>Drop Us A Line</h2>
                <p>Have a question or comment? Use the form below to send us a message or contact us by mail.</p>
                <p id="error"></p>
                </div>

            <div class="forma" action="non-Ajax form handler" style="height:300px">

                  <input type="text" class="input-field" id="customer" placeholder="Customer service" maxlength="20" minlength="4" name="ime"><br>
                  <input type="text" id="mail" class="input-field" name="mail" placeholder="your@email.com" onkeyup='checkEmail()'><br>
                  <input type="file" class="input-field" id="fileUpload"><br>
                  <textarea type="text" id="message" class="input-field" placeholder="How can we help?" name="help"></textarea>
                  <button type="submit" onclick="submit()"> Post Comment </button>
                  <div id='email-validity'></div>
            </div>

            <script>
                  function checkEmail() {

                        const xmlhttp = new XMLHttpRequest();

                        xmlhttp.onload = function() {
                              const myObj = JSON.parse(this.responseText);
                              let message = '';
                              if (!myObj.valid) {
                                    message = 'Invalid email';
                              }
                              document.getElementById("email-validity").innerHTML = message;
                        }
                        xmlhttp.open("GET", "check-email.php?email=" + document.getElementById('mail').value);
                        xmlhttp.send();
                  }
            </script>

            <script src="sc.js"> </script>
</main>
            <?php include_once 'footer.php'; ?>


</html>