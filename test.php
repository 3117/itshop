<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">

<body>
    <?php include_once 'header.php'; ?>
    <section class="images_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="first_image">
                        <div class="image_info">
                            <span>MIDWEEK DEALS</span>
                            <h2>SMARTPHONES <br> ACCESSORIES</h2>
                            <h5>Up to 40% off</h5>
                            <button class="btn">Shop Now <i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 pl-0">
                    <div class="second_image">
                        <div class="image_info">
                            <span>LIMITED STOCK</span>
                            <h4>MACBOOK AIR</h4>
                            <h5>starting from <b>$900.99</b></h5>
                            <button class="btn">Shop Now <i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>
                    <div class="third_image">
                        <div class="image_info">
                            <span>DISCOVER</span>
                            <h4>BEAT PRO X3</h4>
                            <h5>$259.99 <b>$900.99</b></h5>
                            <button class="btn">Shop Now <i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </section>
    <section id="sponsors">
        <img src="quantox.png" alt="" class="sponsors-img">
        <img src="quantox.png" alt="" class="sponsors-img">
        <img src="quantox.png" alt="" class="sponsors-img">
        <img src="quantox.png" alt="" class="sponsors-img">
        <img src="quantox.png" alt="" class="sponsors-img">
    </section>

    <?php include_once 'footer.php'; ?>
</body>

</html>