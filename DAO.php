<?php
require_once 'db.php';

class DAO {
	private $db;

	// za 2. nacin resenja
	//private $INSERT_AUTOMOBILI = "INSERT INTO automobili (id_marke, cena, godiste,danKupovine) VALUES (?,?, ?, ?)";
	//private $UPDATE_AUTOMOBILI = "UPDATE automobili SET id_marke = ?, cena = ?, godiste = ?,danKupovine = ? WHERE id_automobila = ?";
	//private $DELETE_AUTOMOBIL = "DELETE  FROM automobili WHERE id_automobila = ?";
	//private $SELECT_AUTOMOBIL_BY_ID = "SELECT * FROM automobili WHERE id_automobila = ?";	
	
	private $SELECT_PRODUCTS = "SELECT p.id as productId, p.name as productName, p.active, b.name as brandName, b.id as brandId FROM products p JOIN brands b ON b.id = p.id_brand ORDER BY p.id ASC LIMIT ?";
	private $SELECT_ORDERS= "SELECT * FROM orders";
	private $SELECT_ORDERS_DETAILS= "SELECT * FROM ordersdetails";
	//private $SELECT_AUTOMOBIL_BY_DANKUPOVINE = "SELECT * FROM automobili where danKUpovine = ?";
	//private $SELECT_AUTOMOBIL_BY_GODISTE = "SELECT * FROM automobili where godiste = ?";
	
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function selectProducts($n)
	{
		
		$statement = $this->db->prepare($this->SELECT_PRODUCTS);
		$statement->bindValue(1, $n, PDO::PARAM_INT);
		$statement->execute();
		
		$result = $statement->fetchAll();
		//var_dump($result);
		return $result;
	}

	
	public function insertAutomobil($marka, $cena, $godiste, $danKupovine)
	{
		
		$statement = $this->db->prepare($this->INSERT_AUTOMOBILI);
		$statement->bindValue(1, $marka);
		$statement->bindValue(2, $cena);
		$statement->bindValue(3, $godiste);
		$statement->bindValue(4, $danKupovine);
		
		$statement->execute();
	}

	public function updateAutomobil($marka, $cena, $godiste, $danKupovine, $id)
	{
		
		$statement = $this->db->prepare($this->UPDATE_AUTOMOBILI);
		$statement->bindValue(1, $marka);
		$statement->bindValue(2, $cena);
		$statement->bindValue(3, $godiste);
		$statement->bindValue(4, $danKupovine);
		$statement->bindValue(5, $id);
		
		$statement->execute();
	}

	public function deleteAutomobilById($id)
	{
		$statement = $this->db->prepare($this->DELETE_AUTOMOBIL);
		$statement->bindValue(1, $id);
		
		$statement->execute();
	}

	public function selectAautomobiliByDanKupovine($danKupovine)
	{
		$statement = $this->db->prepare($this->SELECT_AUTOMOBIL_BY_DANKUPOVINE);
		$statement->bindValue(1, $danKupovine);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function selectAautomobiliByGodiste($godiste)
	{
		$statement = $this->db->prepare($this->SELECT_AUTOMOBIL_BY_GODISTE);
		$statement->bindValue(1, $godiste);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function selectAutomobilByID($id)
	{
		$statement = $this->db->prepare($this->SELECT_AUTOMOBIL_BY_ID);
		$statement->bindValue(1, $id);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
	public function selectOrders()
	{
		$statement = $this->db->prepare($this->SELECT_ORDERS);

		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}
	public function selectOrdersDetails()
	{
		$statement = $this->db->prepare($this->SELECT_ORDERS_DETAILS);

		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}
}

?>
