<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">

<body>

    <section class="header_info">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 header_info_left">
                    <ul>
                        <li><a href="#">Shipping</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Track Order</a></li>
                    </ul>
                </div>
                <div class="col-xl-6 header_info_right">
                    <i class="fa fa-check-square-o"></i>Free 30 Day Money Back Guarantee
                    <nav class="navbar navbar-expand-lg">
                        <ul class="navbar-nav mr-auto">

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    USD
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">USD</a>
                                    <a class="dropdown-item" href="#">EUR</a>
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    English
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Serbian</a>
                                    <a class="dropdown-item" href="#">German</a>
                                </div>
                            </li>


                        </ul>

                    </nav>
                </div>
            </div>

        </div>
    </section>
    <section class="company_info_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 header_logo_section">
                    <img src="img/logo.png" />
                </div>
                <div class="col-lg-2 header_mail_section">
                    <div>
                        <span>Send us a message</span>
                        <a href="mail to:demo@demo.com">demo@demo.com</a>
                    </div>

                </div>
                <div class="col-lg-3 header_tel_section">
                    <div>
                        <span>Need help? Call us:</span>
                        <a href="tel:demo@demo.com">012 345 6789</a>
                    </div>
                </div>
                <div class="col-lg-4 header_icons_section">
                    <i class="fa fa-user-o"></i>
                    <i class="fa fa-heart-o"></i> 0
                    <i class="fa fa-shopping-bag"></i> 0
                </div>
            </div>
        </div>
    </section>

    <section class="header_menu_section">
        <div class="container">


            <nav class="navbar navbar-expand-lg">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="header_menu">
                    <ul class="navbar-nav mr-auto">
                        <li class="categories_btn">
                            <button class="btn">
                                BROWSE CATEGORIES
                                <span class="navbar_icon">
                                    <i></i>
                                    <i></i>
                                    <i></i>
                                </span>
                            </button>
                        </li>
                        <li class="nav-item active dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                HOME
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">BLOGS</a>
                                <a class="dropdown-item" href="#">CONTACT</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                SHOP
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">BLOGS</a>
                                <a class="dropdown-item" href="#">CONTACT</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                PAGES
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">HOME</a>
                                <a class="dropdown-item" href="#">BLOGS</a>
                                <a class="dropdown-item" href="#">CONTACT</a>
                                <a class="dropdown-item" href="#">ABOUT US</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">BLOGS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONTACT</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0 search_form">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search our catalog" aria-label="Search"><i class="fa fa-search"></i>
                    </form>
                </div>
            </nav>
        </div>
    </section>
    <div>


    </div>
    </section>
</body>

</html>