<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">

    <title>SHOP</title>
    <?php include_once 'header.php'; ?>
</head>
<h4>SEARCH RESULTS</h4>

<div class="wrapper">
      <div id="search-container">
        <input
          type="search"
          id="search-input"
          placeholder="Search product name here.."
        />
        <button id="search">Search</button>
      </div>
      <div id="buttons">
        <input type="checkbox" id='all' name="checkboxAll" onclick="filterProduct()">All
        <input type="checkbox" id='mobile' name="checkboxMobile" onclick="filterProduct()">Mobile
        <input type="checkbox" id='tablets' name="checkboxTablets" onclick="filterProduct()">Tablets
        <input type="checkbox" id='airConditioners' name="checkboxAirConditioners" onclick="filterProduct()">Air conditioners
        </button>
      </div>
      <div id="products"></div>
    </div>
    <script src="filter.js"></script>
  </body>
</html>

<?php
 require_once 'DAO.php';
 $dao = new DAO();
$products = $dao->selectProducts(4);

?>



              


<script src="sc.js"> </script>
    <?php include_once 'footer.php'; ?>

</body>

</html>