<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">

    <title>SHOP</title>
    <?php include_once 'header.php'; ?>
</head>

<body>
    <header>
        <div class="search-box">
            <form action="" onsubmit="return false">
                <input type="text" id="search" placeholder="Product search">
            </form>
        </div>
        <div class="filer-box">
            <a href="#" class="btn active" data-filter="all">ALL</a>
            <a href="#" class="btn " data-filter="laptops">Laptops</a>
            <a href="#" class="btn " data-filter="mouse">Mouse</a>
            <a href="#" class="btn " data-filter="mac">iMac</a>

        </div>
        <div>
            <h2>BRAND</h2>
  <input type="checkbox" id="brand-huawei" value="1" checked>
  <label for="brand-huawei"> Huawei</label><br>
  <input type="checkbox" id="brand-honor" value="2" checked>
  <label for="brand-honor"> Honor</label><br>
  <input type="checkbox" id="brand-apple" value="3" checked>
  <label for="brand-apple"> Apple</label><br>
        </div>
        <br>
        <div>
            <h2>Manifacture</h2>
  <input type="checkbox" id="man-huawei" value="1" checked>
  <label for="brand-huawei"> Huawei</label><br>
    </header>
  
    <main>
        <div class="slide-container" style="display: flex;">
            <section class="container" id="store-product">
                <!-- <div class="store-product mobiles">
                    <img src="img/s2.jpeg" alt="">
                    <div class="product-details">
                        <h2>Mobile Product</h2>
                        <p><span>$250.99</span> 199.99$</p>
                    </div>

                </div>
                <div class="store-product mobiles">
                    <img src="img/s3.jpeg" alt="">
                    <div class="product-details">
                        <h2>Mobile Product</h2>
                        <p><span>$250.99</span> 199.99$</p>
                    </div>


                </div>
                <div class="store-product mobiles">
                    <img src="img/s2.jpeg" alt="">
                    <div class="product-details">
                        <h2>Mobile Product</h2>
                        <p><span>$250.99</span> 199.99$</p>
                    </div>


                </div>
                <div class="store-product mouse">
                    <img src="img/mouse.webp" alt="" style="height: 180px">
                    <div class="product-details">
                        <h2>Mobile Product</h2>
                        <p><span>$250.99</span> 199.99$</p>
                    </div>


                </div>
                <div class="store-product mac">
                    <img src="img/mac.jpeg" alt="" style="height: 180px">
                    <div class="product-details">
                        <h2>Mobile Product</h2>
                        <p><span>$250.99</span> 199.99$</p>
                    </div>


                </div>
                <div class="store-product mobiles">
                    <img src="img/s3.jpeg" alt="">
                    <div class="product-details">
                        <h2>Mobile Product</h2>
                        <p><span>$250.99</span> 199.99$</p>
                    </div>


                </div> -->



            </section>

        </div>
        <div id="products" >
           


        </div>

    </main>
    
    <script src="sc.js"> </script>
    <?php include_once 'footer.php'; ?>

</body>

</html>

<?php




require_once 'DAO.php';
$dao = new DAO();
$productsList= $dao->selectProducts(6);

//var_dump($productsList);


// $productsList = [
//     [
//         'type' => 'laptop',
//         'name' => 'Lenovo 23',
//         'active' => 'true'
//     ],
//     [
//         'type' => 'laptop',
//         'name' => 'Acer',
//         'active' => 'false'
        
//     ],
//     [
//         'type' => 'mobile',
//         'name' => 'iphone 7',
//         'active' => 'true'
//     ],
//     [
//         'type' => 'mobile',
//         'name' => 'samsung galaxy',
//         'active' => 'true'
//     ],
// ];
?>


<script>

    let productsList = <?php echo json_encode($productsList); ?>;

    var brandsChecked = [2, 1];

    getFilteredData();
//debugger;


const honorCheckbox = document.getElementById('brand-honor');

honorCheckbox.addEventListener('change', (event) => {
  if (!event.currentTarget.checked) {
    brandsChecked = brandsChecked.filter(e => e !== 1);
  } else {
      brandsChecked.push(1);
  }
  getFilteredData();
})


const huaweiCheckbox = document.getElementById('brand-huawei');

huaweiCheckbox.addEventListener('change', (event) => {
  if (! event.currentTarget.checked) {
    brandsChecked = brandsChecked.filter(e => e !== 2);
  } else {
      brandsChecked.push(2);
  }
  getFilteredData();
})

function getFilteredData() {
    document.getElementById('products').innerHTML  = '';
    for(let i=0;i<productsList.length;i++){
        if(productsList[i].active == "1"){ 
  //  debugger;
            if (brandsChecked.includes(Number(productsList[i].brandId))) {
  //  debugger;
                document.getElementById('products').innerHTML += productsList[i].productName + '<br>';
            }
        }
    }
}

</script>